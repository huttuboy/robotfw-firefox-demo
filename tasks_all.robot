*** Settings ***
Documentation	Tests
...				
...				Testataan lupapiste.fi -sivustoa
...				
Resource			resource.robot
Test Setup			Initialize Setup
Test Teardown		${TEARDOWN}


*** Test Cases ***
Login page test
	[Documentation]			Test login page opening
	Click Link				//a[contains(., 'Kirjaudu')]
	Location Should Be		${LOGIN_URL}
	#teardown override
	[Teardown]				${TEARDOWN}

Professional page test
	[Documentation]			Test professional page opening
	Click Element			//span[contains(.,'Valikko')]
	Click Link				//a[contains(., 'Ammattilainen')]
	Location Should Be		${PRO_URL}

Search test 1
	[Documentation]			Testing search with pressing Enter
	#override setup
	[Setup]					Open Browser	${LOGIN_URL}	${BROWSER}
	Set Selenium Speed		${SPEED}
	Click Element			//span[contains(.,'Haku')]
	Input Text				//*[@id='search-field']		kukkuu
	Press Keys				//*[@id='search-field']		RETURN
	Location Should Be		${MAIN_URL}?s=kukkuu

Search test 2
	[Documentation]			Testing search with pushing Button
	Click Element			//span[contains(.,'Haku')]
	Input Text				//*[@id='search-field']		hihhei
	Click Element			//*[@id='searchsubmit']
	Location Should Be		${MAIN_URL}?s=hihhei

Login test
	[Documentation]			Testing login with faulty credentials
	Input Text				//*[@id='login-username']	asd
	Click Element			//*[@id='next-button']
	Input Text				//*[@id='login-password']	dsa
	Click Element			//*[@id='login-button']
	Element Should Contain	//*[@id='login-message']	Tunnus tai salasana on v

