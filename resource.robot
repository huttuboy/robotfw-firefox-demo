*** Settings ***
Documentation	Resource file with reusable variables and keywords.
...
...			Taalta loytyy keywordsit ja variablet testeihin.
...
Library		SeleniumLibrary

*** Variables ***
${MAIN_URL}		https://www.lupapiste.fi/
${LOGIN_URL}	https://www.lupapiste.fi/kirjaudu
${PRO_URL}		https://www.lupapiste.fi/ammattilainen
${BROWSER}		firefox
${SPEED}		1
${TEARDOWN}		Close All Browsers

*** Keywords ***
Initialize Setup
	Open Browser		${MAIN_URL}		${BROWSER}
	Set Selenium Speed	${SPEED}

Login Page
	Open Browser	${LOGIN_URL}	${BROWSER}
